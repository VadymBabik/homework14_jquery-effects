"use strict";

$(".top_menu__nav--list a").click(function (e) {
  let linkHref = $(this).attr("href");
  let idElement = linkHref.substr(linkHref.indexOf("#"));
  $("html, body").animate(
    {
      scrollTop: $(idElement).offset().top,
    },
    3000
  );
  return false;
});
//

$(function () {
  $(".slide-toggle").click(function () {
    $(".top-rated__gallery").slideToggle(1000);
  });
});

//
let btn = $("#button");

$(window).scroll(function () {
  if ($(window).scrollTop() > $(window).height()) {
    btn.addClass("show");
  } else {
    btn.removeClass("show");
  }
});

$(function () {
  btn.on("click", function (e) {
    e.preventDefault();
    $("html, body").animate({ scrollTop: 0 }, "200");
  });
});
